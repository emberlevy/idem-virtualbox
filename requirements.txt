idem >= 7
idem-bsd; "BSD" in sys_platform
idem-darwin; sys_platform == "darwin"
idem-linux; sys_platform == "linux"
idem-solaris; "sunos" in sys_platform
idem-windows; sys_platform == 'win32'
pop >= 13
pop-config>=6
virtualbox>=2.0.0
